## Description
- mydns auto-updater using python 

## Install
1. install python3(>=3.6), python3-pip, git
   - Debian example:
```
# apt install python3, python3-pip, git
```
2. install pipenv on pip3
```
$ pip3 install pipenv
```
3. clone this repository from git
```
$ git clone https://gitlab.com/tetsup/mydns.git
```
4. create pipenv virtual environment
```
$ pipenv install
```
5. copy and edit settings
   - mydns.sample.ini --> mydns.ini
6. check software working correctly
```
$ pipenv run python mydns.py
```
7. copy and edit service file
   - mydns.sample.service --> mydns.service
8. exec auto-install shell script(if using systemd)
```
# ./register_systemd.sh
```
9. (if nessessary)stop service
```
# systemctl stop mydns
# systemctl disable mydns
```

