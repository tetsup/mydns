import requests
import configparser
from contextlib import closing
from logging.config import fileConfig
from logging import getLogger
import re
import time
import signal
import sys

config = configparser.ConfigParser()
config.read('mydns.ini')
fileConfig('logging.ini')
logger = getLogger('root')

def main():
    logger.info('start')
    signal.signal(signal.SIGTERM, sigterm)
    logger.debug('sigterm setting ok')
    ip = ''
    last_updated = None
    while True:
        logger.debug('loop start')
        try:
            ip_new = check_ip()
            if last_updated is None:
                logger.info('initialize(%s)' % ip_new)
                last_updated = update_mydns(last_updated)
            elif time.time() - last_updated >= int(config['mydns']['interval_days']) * 86400:
                logger.info('interval update(%s)' % ip_new)
                last_updated = update_mydns(last_updated)
            elif ip != ip_new:
                logger.info('ip chenged(%s)' % ip_new)
                last_updated = update_mydns(last_updated)
            ip = ip_new
            time.sleep(int(config['ipchk']['interval_seconds']))
        except Exception:
            logger.exception()

def sigterm(signum, frame):
    logger.info('sigterm detected')
    signal.signal(signal.SIGTERM, signal.SIG_DFL)
    sys.exit()

def check_ip():
    res = requests.get(config['ipchk']['url'])
    if res.status_code == 200:
        return match_ip(res.text)
    else:
        logger.warning('failed checking')
        return ''

def match_ip(ip):
    matched = re.search(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', ip)
    return matched[1]

def update_mydns(last_updated):
    res = requests.get(config['mydns']['url'], auth=(config['mydns']['user'], config['mydns']['pass']))
    if res.status_code == 200:
        logger.info('successfully updated')
        return time.time()
    else:
        logger.warning('failed updating(status: %s)' % res.status_code)
        return last_updated

if __name__ == '__main__':
    main()

